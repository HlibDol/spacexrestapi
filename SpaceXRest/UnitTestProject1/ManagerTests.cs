﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SpaceXRest;
using SpaceXRest.Managers;
using SpaceXRest.Services;
using System;
using System.Threading.Tasks;

namespace UnitTestProject1
{
    [TestClass]
    public class ManagerTests
    {
        [TestMethod]
        public async Task ManagerGetDataTest()
        {
            // Arrange
            var service = new Mock<ISpaceXService>();
            service.Setup(x => x.GetSpaceXRemoteData()).Returns(Task.FromResult("Some data from response"));

            var manager = new SpaceXManager(service.Object);

            // Act
            var data = await manager.GetSpaceXRemoteData();

            // Assert
            Assert.IsNotNull(data);
        }

        [TestMethod]
        public async Task ManagerHandlesExceptionTest()
        {
            // Arrange
            var service = new Mock<ISpaceXService>();
            service.Setup(x => x.GetSpaceXRemoteData()).Throws(new ArgumentNullException());

            var manager = new SpaceXManager(service.Object);

            // Act
            var data = await manager.GetSpaceXRemoteData();

            // Assert
            Assert.AreEqual(Constants.SomethingWentWrong, data);
        }
    }
}
