﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SpaceXRest.Managers;
using SpaceXRest.ViewModels;
using System.Threading.Tasks;

namespace UnitTestProject1
{
    [TestClass]
    public class ViewModelTests
    {
        [TestMethod]
        public async Task InitSpaceXInfo()
        {
            // Arrange
            var manager = new Mock<ISpaceXManager>();
            var vm = new MainViewModel(manager.Object);

            // Act
            await vm.Initialize();
            var info = vm.SpaceXInfo;

            // Assert
            Assert.IsNotNull(info); 
        }
    }
}
