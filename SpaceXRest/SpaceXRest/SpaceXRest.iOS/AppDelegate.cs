﻿using Foundation;
using MvvmCross.Platforms.Ios.Core;

namespace SpaceXRest.iOS
{
    [Register("AppDelegate")]
    public class AppDelegate : MvxApplicationDelegate<MvxIosSetup<App>, App>
    {
    }
}
