﻿using MvvmCross.Platforms.Ios.Presenters.Attributes;
using MvvmCross.Platforms.Ios.Views;
using SpaceXRest.ViewModels;

namespace SpaceXRest.iOS.Views
{
    [MvxRootPresentation]
    public partial class MainViewController : MvxViewController<MainViewModel>
    {
        public MainViewController() : base("MainViewController", null)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            var set = CreateBindingSet();

            set.Bind(TextView).To(vm => vm.SpaceXInfo);

            set.Apply();
        }
    }
}

