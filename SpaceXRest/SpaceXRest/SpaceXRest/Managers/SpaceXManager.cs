﻿using SpaceXRest.Services;
using System;
using System.Threading.Tasks;

namespace SpaceXRest.Managers
{
    public class SpaceXManager : ISpaceXManager
    {
        private readonly ISpaceXService _service;

        public SpaceXManager(ISpaceXService service)
        {
            _service = service;
        }

        public async Task<string> GetSpaceXRemoteData()
        {
            string info;

            try
            {
                info = await _service.GetSpaceXRemoteData();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);

                info = Constants.SomethingWentWrong;
            }

            return info;
        }
    }
}
