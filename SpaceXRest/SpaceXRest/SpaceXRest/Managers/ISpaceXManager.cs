﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SpaceXRest.Managers
{
    public interface ISpaceXManager
    {
        Task<string> GetSpaceXRemoteData();
    }
}
