﻿using MvvmCross;
using MvvmCross.IoC;
using MvvmCross.ViewModels;
using SpaceXRest.Managers;
using SpaceXRest.Services;
using SpaceXRest.ViewModels;

namespace SpaceXRest
{
    public class App : MvxApplication
    {
        public override void Initialize()
        {
            RegisterDependencies();

            RegisterAppStart<MainViewModel>();
        }

        private void RegisterDependencies()
        {
            Mvx.IoCProvider.RegisterType<ISpaceXService, SpaceXService>();
            Mvx.IoCProvider.RegisterType<ISpaceXManager, SpaceXManager>();
        }
    }
}
