﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace SpaceXRest.Services
{
    public class SpaceXService : ISpaceXService
    {
        private const string Url = "https://api.spacexdata.com/v4/launches/latest";

        private readonly HttpClient _httpClient;

        public SpaceXService()
        {
            _httpClient = new HttpClient
            {
                Timeout = TimeSpan.FromSeconds(20)
            };
        }

        public async Task<string> GetSpaceXRemoteData()
        {
            var response = await _httpClient.GetAsync(Url);

            return await HandleHttpResponse(response);
        }

        private async Task<string> HandleHttpResponse(HttpResponseMessage response)
        {
            if(response == null || response.Content == null || !response.IsSuccessStatusCode)
            {
                throw new ArgumentNullException();
            }

            return await response.Content.ReadAsStringAsync();
        }
    }
}
