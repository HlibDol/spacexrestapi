﻿using System.Threading.Tasks;

namespace SpaceXRest.Services
{
    public interface ISpaceXService
    {
        Task<string> GetSpaceXRemoteData();
    }
}
