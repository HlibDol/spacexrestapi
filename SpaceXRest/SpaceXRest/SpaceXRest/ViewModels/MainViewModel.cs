﻿using MvvmCross.ViewModels;
using SpaceXRest.Managers;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SpaceXRest.ViewModels
{
    public class MainViewModel : MvxViewModel
    {
        private readonly ISpaceXManager _spaceXManager;

        public MainViewModel(ISpaceXManager spaceXManager)
        {
            _spaceXManager = spaceXManager;
        }

        private string _spaceXInfo;
        public string SpaceXInfo
        {
            get => _spaceXInfo;
            set => SetProperty(ref _spaceXInfo, value);
        }

        public override async Task Initialize()
        {
            SpaceXInfo = await _spaceXManager.GetSpaceXRemoteData();
        }
    }
}
