﻿using Android.App;
using Android.Content.PM;
using MvvmCross.Platforms.Android.Views;
using MvvmCross.Platforms.Android.Core;

namespace SpaceXRest.Droid
{
    [Activity(
        Label = "Splash"
        , MainLauncher = true
        , NoHistory = true
        , ScreenOrientation = ScreenOrientation.Portrait)]
    public class SplashScreen : MvxSplashScreenActivity<MvxAndroidSetup<App>, SpaceXRest.App>
    {
        public SplashScreen()
             : base(Resource.Layout.Splash)
        {
        }
    }
}