﻿using Android.App;
using Android.OS;
using Android.Widget;
using MvvmCross.Platforms.Android.Presenters.Attributes;
using MvvmCross.Platforms.Android.Views;
using SpaceXRest.ViewModels;

namespace SpaceXRest.Droid.Views
{
    [MvxActivityPresentation]
    [Activity(Label = "View")]
    public class MainView : MvxActivity<MainViewModel>
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.Main);

            var tv = FindViewById<TextView>(Resource.Id.textView1);

            var bindingSet = this.CreateBindingSet();

            bindingSet.Bind(tv).For(v => v.Text).To(vm => vm.SpaceXInfo).TwoWay();

            bindingSet.Apply();
        }
    }
}